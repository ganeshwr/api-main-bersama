/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

Route.group(() => {
  Route.group(() => {
    // Auth
    Route.post('/register', 'AuthController.register').as('auth.register')
    Route.post('/login', 'AuthController.login').as('auth.login')
    Route.get('/otp-confirmation/:otp/:email', 'AuthController.verifyOtp').as('auth.verify.otp')
    Route.post('/request-new-otp', 'AuthController.requestNewOtp').as('auth.request.new.otp')
    Route.post('/logout', 'AuthController.logout').as('auth.logout').middleware('auth') // addition

    // Venue
    Route.resource('venues', 'VenuesController').apiOnly().middleware({
      store: ['auth'],
      update: ['auth'],
      destroy: ['auth'],
    })
    Route.post('/venues/:id/bookings', 'VenuesController.booking').as('venues.booking').middleware('auth')

    // Field
    Route.resource('venues.fields', 'FieldsController').apiOnly().middleware({
      store: ['auth'],
      update: ['auth'],
      destroy: ['auth'],
    }).except(['index', 'show'])
    Route.get('/fields', 'FieldsController.index').as('fields.index')
    Route.get('/fields/:id', 'FieldsController.show').as('fields.show')

    // Booking
    Route.group(() => {
      Route.get('/bookings', 'BookingsController.index').as('bookings.index')
      Route.get('/bookings/:id', 'BookingsController.show').as('bookings.show')
      Route.put('/bookings/:id/join', 'BookingsController.join').as('bookings.join')
      Route.put('/bookings/:id/unjoin', 'BookingsController.unjoin').as('bookings.unjoin')
      Route.get('/schedules', 'BookingsController.schedules').as('bookings.schedules')
    }).middleware('auth')
  }).prefix('/v1')
}).prefix('/api')