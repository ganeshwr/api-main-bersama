import Mail from '@ioc:Adonis/Addons/Mail'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import OtpCode from 'App/Models/OtpCode'
import User from 'App/Models/User'
import LoginValidator from 'App/Validators/LoginValidator'
import RegisterValidator from 'App/Validators/RegisterValidator'
import RequestNewOtpValidator from 'App/Validators/RequestNewOtpValidator'
import VerifyOtpValidator from 'App/Validators/VerifyOtpValidator'
import { DateTime } from 'luxon'

export default class AuthController {
    /**
     * 
     * @param param0 
     * @returns 
     */
    public async register({request, response}: HttpContextContract) {
        const payload = await request.validate(RegisterValidator)

        const {email} = payload
        const generateOtp = Math.floor(100000 + Math.random() * 900000 )

        const sendMail = await Mail.send((message) => {
            message
                .from('noreply-registration@sanberdev.com')
                .to(email)
                .subject('Main Bersama : OTP Verification')
                .htmlView('emails/otp_verification', {
                    otpNumber: generateOtp,
                    verifyUrl: request.host()+`/api/v1/otp-confirmation/${generateOtp}/${payload.email}`
                })
        })

        if(sendMail) {
            const newUser = await User.create(payload)
            const nextOneHour = DateTime.now().plus({hours:1})
            
            await OtpCode.create({
                otp: generateOtp,
                userId: newUser.id,
                expiresAt: nextOneHour
            })

            return response.created({
                message: "Please check your email for the OTP verification code to complete the registration."
            })
        } else {
            return response.badGateway({
                message: "Failed to send email."
            })
        }
        
    }

    public async login({request, response, auth}: HttpContextContract) {
        const payload = await request.validate(LoginValidator)

        const checkUser = await User.findBy('email', payload.email)
        if(checkUser != null && checkUser.isVerified == false) {
            return response.badRequest({
                message: "User is not verified. Please verify OTP first."
            })
        }
        
        const token = await auth.use('api').attempt(payload.email, payload.password, {
            expiresIn: '7days'
        })

        return response.ok({
            message: "Logged in successfully.",
            token
        })
    }

    public async logout({request,response, auth}: HttpContextContract) {
        if(!request.headers().authorization) {
            return response.unauthorized({
                message: "Token not valid."
            })
        }
        
        await auth.use('api').authenticate()
        await auth.use('api').revoke()

        return response.ok({
            message: "Logged out successfully."
        })
    }

    public async verifyOtp({request, response}: HttpContextContract) {
        const {otp, email} = request.params()

        const userModel = await User.findByOrFail('email', email)
        const otpModel = await OtpCode.findBy('otp', otp)

        if(userModel.isVerified) {
            return response.badRequest({
                message: "User is already verified."
            })
        }

        if(otpModel == null || otpModel.isValid == false) {
            return response.badRequest({
                message: "Invalid OTP."
            })
        }

        if(otpModel != null && otpModel.expiresAt < DateTime.now()) {
            return response.badRequest({
                message: "OTP expired."
            })
        }
        
        if(userModel.id == otpModel.userId) {
            userModel.isVerified = true
            otpModel.isValid = false
            await userModel.save()
            await otpModel.save()

            return response.ok({
                message: "OTP verification success."
            })
        }
    }

    // Request new registration OTP
    public async requestNewOtp({request, response}: HttpContextContract) {
        const payload = await request.validate(RequestNewOtpValidator)

        const checkUser = await User.findBy('email', payload.email)

        if(checkUser == null) {
            return response.badRequest({
                message: "User not found."
            })
        }

        const generateOtp = Math.floor(100000 + Math.random() * 900000 )
        const nextOneHour = DateTime.now().plus({hours:1})

        await OtpCode.create({
            otp: generateOtp,
            userId: checkUser.id,
            expiresAt: nextOneHour
        })
        
        await Mail.send((message) => {
            message
                .from('noreply-registration@sanberdev.com')
                .to(payload.email)
                .subject('Main Bersama : OTP Verification')
                .htmlView('emails/otp_verification', {
                    otpNumber: generateOtp,
                    verifyUrl: request.host()+`/api/v1/otp-confirmation/${generateOtp}/${payload.email}`
                })
        })

        return response.ok({
            message: "New OTP code sent."
        })
    }
}
