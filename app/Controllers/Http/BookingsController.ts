import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Booking from 'App/Models/Booking'

export default class BookingsController {
  public async index ({response}: HttpContextContract) {
    // const bookings = await auth.user?.related('venues').query().preload('fields', ((fieldsQuery) => {
    //   fieldsQuery.preload('bookings')
    // }))

    const bookings = await Booking.all()

    return response.ok({
      bookings
    })
  }

  public async show ({request, response}: HttpContextContract) {
    const {id} = request.params()

    const booking = await Booking.find(id)
    if(booking == null) {
      return response.notFound({
        message: "Booking not found."
      })
    }

    return response.ok({
      booking
    })
  }

  public async join ({request, response, auth}: HttpContextContract) {
    const {id} = request.params()

    const booking = await Booking.findOrFail(id)

    await booking.related('users').attach([auth.user!.id])

    return response.ok({
      message: "Successfully join booking."
    })
  }

  public async unjoin ({request, response, auth}: HttpContextContract) {
    const {id} = request.params()

    const booking = await Booking.findOrFail(id)

    await booking.related('users').detach([auth.user!.id])

    return response.ok({
      message: "Successfully unjoin booking."
    })
  }

  public async schedules({response, auth}: HttpContextContract) {
    const schedules = await auth.user?.related('bookings').query()

    return response.ok({
      schedules
    })
  }
}
