import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Booking from 'App/Models/Booking'
import Venue from 'App/Models/Venue'
import BookingVenueValidator from 'App/Validators/BookingVenueValidator'
import StoreVenueValidator from 'App/Validators/StoreVenueValidator'
import UpdateVenueValidator from 'App/Validators/UpdateVenueValidator'

export default class VenuesController {
  public async index ({response}: HttpContextContract) {
    const venues = await Venue.query().preload('owner').preload('fields')
    return response.ok({
      venues
    })
  }

  public async store ({request, response, auth}: HttpContextContract) {
    const payload = await request.validate(StoreVenueValidator)

    await auth.user?.related('venues').create(payload)

    return response.created({
      message: "New venue added."
    })
  }

  public async show ({request, response}: HttpContextContract) {
    const {id} = request.params()

    const venue = await Venue.query().preload('owner').preload('fields').where('id', id).firstOrFail()

    return response.ok({
      venue
    })
  }

  public async update ({request, response, auth}: HttpContextContract) {
    const payload = await request.validate(UpdateVenueValidator)
    const {id} = request.params()

    const venue = await auth.user?.related('venues').query().where('id', id).firstOrFail()
    await venue?.merge(payload).save()

    return response.ok({
      message: "Venu data updated."
    })
  }

  public async destroy ({request, response, auth}: HttpContextContract) {
    const {id} = request.params()

    const venue = await auth.user?.related('venues').query().where('id', id).first()
    if(venue == null) {
      return response.notFound({
        message: "Venue not found."
      })
    }

    await venue.delete()

    return response.ok({
      message: "Venue removed."
    })
  }

  public async booking({request, response, auth}: HttpContextContract) {
    const {playDateStart, playDateEnd, fieldId} = await request.validate(BookingVenueValidator)
    
    const newBooking = new Booking()
    newBooking.playDateStart = playDateStart
    newBooking.playDateEnd = playDateEnd
    newBooking.fieldId = fieldId
    newBooking.userIdBooking = auth.user!.id

    await newBooking.save()
    await newBooking.related('users').attach([newBooking.userIdBooking])

    return response.ok({
      message: "Booked."
    })

  }
}
