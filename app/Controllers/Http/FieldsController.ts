import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Field from 'App/Models/Field'
import StoreFieldValidator from 'App/Validators/StoreFieldValidator'
import UpdateFieldValidator from 'App/Validators/UpdateFieldValidator'

export default class FieldsController {
  public async index ({response}: HttpContextContract) {  
    const fields = await Field.query().preload('venue')
    return response.ok({
      fields
    })
  }

  public async store ({request, response, auth}: HttpContextContract) {
    const payload = await request.validate(StoreFieldValidator)
    const {venue_id} = request.params()

    const venue = await auth.user?.related('venues').query().where('id', venue_id).first()
    if(venue == null) {
      return response.notFound({
        message: "Venue not found."
      })
    }

    await venue.related('fields').create(payload)
  
    return response.ok({
      message: `New field added to venue with id ${venue.id}`
    })
  }

  public async show ({request, response}: HttpContextContract) {
    const {id} = request.params()

    const field = await Field.query().preload('venue').where('id', id).first()
    if(field == null) {
      return response.notFound({
        message: "Field not found."
      })
    }

    return response.ok({
      field
    })
  }

  public async update ({request, response, auth}: HttpContextContract) {
    const payload = await request.validate(UpdateFieldValidator)
    const {venue_id, id} = request.params()

    const venue = await auth.user?.related('venues').query().where('id', venue_id).first()
    if(venue == null) {
      return response.notFound({
        message: `Venue with id ${venue_id} not found for user id ${auth.user?.id}.`
      })
    }

    const field = await venue.related('fields').query().where('id', id).first()
    if(field == null) {
      return response.notFound({
        message: "Field not found."
      })
    }
    
    await field.merge(payload).save()
  
    return response.ok({
      message: `Field data updated.`
    })
  }

  public async destroy ({request, response, auth}: HttpContextContract) {
    const {venue_id, id} = request.params()

    const venue = await auth.user?.related('venues').query().where('id', venue_id).first()
    if(venue == null) {
      return response.notFound({
        message: `Venue with id ${venue_id} not found for user id ${auth.user?.id}.`
      })
    }

    const field = await venue.related('fields').query().where('id', id).first()
    if(field == null) {
      return response.notFound({
        message: "Field not found."
      })
    }
    
    await field.delete()
  
    return response.ok({
      message: `Field removed.`
    })
  }
}
