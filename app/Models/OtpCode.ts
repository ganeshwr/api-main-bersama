import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class OtpCode extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public otp: number

  @column()
  public userId: number

  @column()
  public isValid: boolean

  @column.dateTime()
  public expiresAt: DateTime

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
