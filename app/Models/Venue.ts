import { DateTime } from 'luxon'
import { BaseModel, BelongsTo, belongsTo, column, HasMany, hasMany, HasManyThrough, hasManyThrough, HasOne, hasOne } from '@ioc:Adonis/Lucid/Orm'
import User from './User'
import Field from './Field'
import Booking from './Booking'

export default class Venue extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public name: string

  @column()
  public address: string

  @column()
  public phone: string

  @column()
  public userId: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @belongsTo(() => User)
  public owner: BelongsTo<typeof User>

  @hasMany(() => Field)
  public fields: HasMany<typeof Field>

  // @hasManyThrough([
  //   () => Booking,
  //   () => Field
  // ])
  // public bookings: HasManyThrough<typeof Booking>
}
